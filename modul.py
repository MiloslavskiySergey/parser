from aiohttp import ClientSession
from bs4 import BeautifulSoup
import re
import asyncio


async def fetch(session, url):  # -> Any:
    async with session.get(url) as response:
        return await response.text()


async def extract_phone_numbers(url):  # -> set:
    async with ClientSession() as session:
        html = await fetch(session, url)
        soup = BeautifulSoup(html, "html.parser")
        phone_numbers = set()

        phone_pattern = re.compile(
            r"(?:\+7|8)\s?\(?\d{3}\)?[\s-]?\d{3}[\s-]?\d{2}[\s-]?\d{2}"
        )

        for text_node in soup.find_all(text=True):
            matches = phone_pattern.findall(text_node)
            phone_numbers.update(matches)

        transformed_numbers = set()
        for number in phone_numbers:
            number = re.sub(r"\D", "", number)
            if number.startswith("7"):
                number = "8" + number[1:]
            transformed_number = "8" + number[1:]
            transformed_numbers.add(transformed_number)

        return transformed_numbers


async def main() -> None:
    urls = ["https://hands.ru/company/about", "https://repetitors.info"]

    for url in urls:
        phone_numbers = await extract_phone_numbers(url)
        print(f"Phone numbers found on {url}:")
        for number in phone_numbers:
            print(number)


if __name__ == "__main__":
    asyncio.run(main())
